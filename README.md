# Reiseplanung #

Was gibt es zu tun? Und was braucht es?


## Schweiz

- [Aktivitäten in Nidwalden](CH-Nidwalden.md)
- [Höllgrotten Baar](CH-Zug-Hoellgrotten.md)
- Autonomes Postauto [Homepage](https://www.postauto.ch/de/smartshuttle-testbetrieb-sitten)
- Rhein Campingplatz Kaiseraugst [Homepage](http://www.camping-kaiseraugst.ch)


## Städtereisen

Städtereisen kosten wenig Zeit und sind leicht an einem verlängertem 
Wochenende durchzuführen. Welche Stadt ist sehenswert? Wie viel kostet 
die Anreise?

- [Berlin](DE-Berlin.md)
- [Barcelone](ES-Barcelona.md)
- [Paris](FR-Paris.md)
- [Amsterdam](NL-Amsterdam.md)


## Erholungsreisen

- Hausbootferien
- Ferien am Meer
- Rheinradweg
- [Chile](CL-Santiago_de_Chile.md)
