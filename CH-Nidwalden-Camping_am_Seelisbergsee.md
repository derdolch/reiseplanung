# Strandbad & Camping am Seelisbergsee

[Homepage](http://www.seelisberg.com/essen-schlafen/essen-und-schlafen/naturcamping.html)

Preis für Nacht     | CHF
--------------------|---
Erwachsene          | 14.00
Kinder 6 - 16 Jahre | 7.00

Infrastruktur und Angebot am Selisbergsee:
- Seebad, zeitweise Badeaufsicht
- Flacher Sandstrand, für Kinder gut geeignet
- Badesteg mit Sprungbrett
- Floss
- Kinderbassin mit Schaukel
- Kinderspielplatz, Wasserspielplatz
- Beach-Volleyballfeld
- Grillplätze mit Rost
- Kabinen, WC- und Duschanlagen
- Ruderbootverleih

Das Baden ist nur vom Strandbadareal her erlaubt.

Gummi- und Paddelboote sind auf dem ganzen See erlaubt. Hunde sind auf
dem ganzen Areal nicht gestattet.

Autofreier Campingplatz nur für Zelte, geöffnet Mitte Mai bis
Anfang/Mitte September. Ca. 100 Plätze stehen zur Verfügung.

Ausstattung: Feuerstellen, Duschen, Waschmaschine + Tumbler,
Geschirrwaschbecken, Gemeinschaftsraum mit TV, Akku-Ladestationen.

Bestellung von Brot und Fleisch beim Seeli-Kiosk möglich,
Internetempfang beim Kiosk.

```
Campingplatz & Strandbad Seeli 
Seelistrasse 4
6377 Seelisberg

Tel. Camping & Badi: 041 820 35 96 (Camping Anmeldung ab 1. Mai möglich)
```
