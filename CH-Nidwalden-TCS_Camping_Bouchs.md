# TCS Camping Buochs

[TCS-Homepage](https://www.tcs.ch/de/camping-reisen/camping/campingplaetze/standorte/buochs-vierwaldstaettersee.php)

Wiedereröffnung nach Umbau: Donnerstag, 25. Mai 2017.

Dieser Campingplatz liegt am Vierwaldstättersee zwischen einem 
Sportplatz und dem Seestrandbad. Auf der grünen Wiese macht man es sich 
gemütlich und der klare See sorgt bei sonnigen Tagen für Erfrischung.
Dies ist der ideale Ausgangspunkt, um Berge, Seen und die zahlreichen
natürlichen Schätze des schönen Mittellandes und der Voralpen zu
erkunden.

Wichtig ! Bevor Sie ihre Online-Buchung vornehmen, beachten Sie bitte
unsere Ermässigungscode. Sie finden diese unten im Reiter «Rabatte».

```
TCS Camping Buochs
6374 Buochs-Ennetbürgen
Tel: +41 41 620 34 74
Fax: +41 41 620 64 84
camping.buochs@tcs.ch
```

Preise 16.7.2017:

Tarife pro Nacht | Nebensaison | Hochsaison 
-----------------|-------------|------------
Kleines Zelt 2x2¹| CHF   10.00 | CHF   12.00 
Standard¹        | CHF   22.00 | CHF   30.00 
Premium¹         | CHF   32.00 | CHF   40.00 
Erwachsene       | CHF   11.00 | CHF   14.00 
Kind 6-15 Jahre  | CHF    5.50 | CHF    7.00 
Hund             | CHF    5.00 | CHF    5.00 
Taxen (pro Erwachsene) | CHF    2.10 | CHF    2.10 


Mietobjekte*     | Nebensaison | Hochsaison 
-----------------|-------------|------------
Pod 2 Personen   | CHF   55.00 | CHF   85.00
Pod 3 Personen   | CHF   65.00 | CHF   85.00
Barrierefreie Bungalows 2 Pers.  | CHF   115.00 | CHF  205.00
Barrierefreie Bungalows 3-4 Pers.| CHF   130.00 | CHF  205.00
