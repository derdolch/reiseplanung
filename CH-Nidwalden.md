# Nidwalden


## Unterkünfte

Preis     | Beschreibung 
----------|---
ab 23 CHF | [TCS Camping Buochs](CH-Nidwalden-TCS_Camping_Bouchs.md)
ab 14 CHF | [Strandbad & Camping am Seelisbergsee](CH-Nidwalden-Camping_am_Seelisbergsee.md)
ab 60 CHF | Gasthaus Gravenorts
ab 28 CHF | Schlafen im Stroh [www.roelli-stroh.ch](http://www.roelli-stroh.ch)
frei      | Schlafen am Strand

## Ausflüge & Aktivitäten
 
Preis       | Beschreibung 
------------|---
Frei        | Schnitzturm Stansstad
ab 15 CHF   | Fahrradvermietung - Velo Frank
Frei        | Geführte Biketour - Velo Frank
ab 5.60 CHF | [Fahrkarten SBB](http://sbb.ch)
ab 5.60 CHF | [Fahrkarten Postauto](http://postauto.ch)
